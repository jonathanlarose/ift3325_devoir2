/**
 * Created by Jonathan on 2017-11-22.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Sender {

    private static String nomMachine;
    private static String nomFichier;
    private static int numPort;
    private static int option;
    private static Socket s;
    private static BufferedReader br;
    private static PrintWriter pw;
    private static boolean erreur = true;

    /**
     * Fonction executable du Sender.
     * @param args Nom_Machine, Numero_Port, Nom_Fichier, Option (0 pour Go-Back-N)
     * @throws IOException
     */
    public static void main(String[] args) throws IOException
    {
        //Assignation des arguments en entrée
        try {
            nomMachine = args[0];
            numPort = Integer.parseInt(args[1]);
            nomFichier = args[2];
            option = Integer.parseInt(args[3]);

            if (option != 0)
                throw new Exception("La connexion doit utiliser Go-Back-N (option <0>)");

        } catch (Exception e) {
            System.out.println(e);
            System.out.print("Sender <Nom_Machine> <Num_Port> <Nom_Fichier> <0>");
            return;
        }

        //Parse du fichier en entree en donnée pour le transfer
        ArrayList<String> paquetDonnees = ParserTrames.parseTrames(nomFichier);

        //Connection : si ca ne fonctionne pas, on quitte
        if(!connect())
            return;

        //Envoit les données puis déconnecte
        send(paquetDonnees);
        deconnect();

        return;
    }

    private static boolean connect(){

        /**
         * Connexion au port :
         * Initialisation du socket, du bufferedReader et printWriter pour la communication.
         * Envoie du premier message (C) pour indiquer qu"on est prêt à commencer l'envoie.
         **/

        try {
            s = new Socket(nomMachine, numPort);
            s.setSoTimeout(3000);
            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            pw = new PrintWriter(s.getOutputStream());

            //Début de la communication
            Trame t = new Trame(Type.C, option,"");
            printToBuffer(t);

            //Attend une confirmation
            Trame confirm = new Trame(br.readLine());
            System.out.println("IN :\t" + confirm.log());
            if(confirm.getType() == Type.A && confirm.getNum() == 0)
                System.out.println();
            else
                throw new TrameException("Réponse invalide du receiver lors de la connexion pour l'envoie.");

        }catch (SocketTimeoutException e){
            System.out.println(e);
            System.out.println("Pas de réponse du receiver pour l'envoie.");
            return false;
        }
        catch (Exception e){
            System.out.println(e);
            return false;
        }

        return true;
    }

    /**
     * Méthode pour fermer la connection et les buffers
     */
    private static void deconnect(){
        try{
            br.close();
            pw.close();
            s.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * Fonction qui envoie le data à l'autre machine
     * @param donnees Les donnees en binaire (sous forme de string) à envoyer
     */
    private static void send(ArrayList<String> donnees)
    {
        int pointeurFenetre = 0;        //Début de la fenêtre des paquets à envoyer
        int pointeurTrame = 0;          //Première Trame de la batch
        int nt = 0;                     //Trame courante
        boolean connect = true;
        boolean endSent = false;        //Dernier paquet du fichier envoyé
        boolean endConfirm = false;     //Dernier paquet de la dernière "batch" de paquet recu
        boolean tramePerdu = false;

        //Envoie du fichier
        while(connect){

            //Envoie de 7 trames consécutives
            Trame t;
            nt = 0;

            for(int j=0; j<=6; j++) {

                nt = (pointeurTrame + j) % 8;

                //Si on a envoyé tous les données
                if(pointeurFenetre + j == donnees.size()) {
                    endSent = true;
                    nt = (nt + 7) % 8;
                    break;
                }
                else{
                    //Crée la trame qui sera envoyé avec les données correspondantes
                    t = new Trame(Type.I, nt, donnees.get(pointeurFenetre + j));


                    //Bug Corrupt
                    if(Math.floor(Math.random()*16) == 7 && erreur)
                        t.corruptTrame();

                    //Bug paquet perdu
                    if(Math.floor(Math.random()*16) == 11 && erreur){
                        System.out.println("OUT :\t" + t.log());
                        continue;
                    }

                    printToBuffer(t);
                }
            }


            //Écoute des réponses du receiver
            tramePerdu = false;
            endConfirm = false;

            while(true) {

                try {
                    s.setSoTimeout(3000);
                    Trame tReponse = new Trame(br.readLine());
                    System.out.println("IN :\t" + tReponse.log());

                    //Si une réponse est corrompue, on l'ignore tout simplement
                    if (!tReponse.estValide()) {
                        System.out.println("\t\t" + tReponse.log());
                        continue;
                    }

                    //Si la réponse est un accusé de réception
                    if(tReponse.getType() == Type.A){

                        //On attend seulement l'accusé de réception du dernier paquet envoyé
                        if(tReponse.getNum() == (nt+1)%8){
                            endConfirm = true;
                            pointeurFenetre += 7;
                            pointeurTrame = (pointeurTrame + 7)%8;
                            break;
                        }
                    }
                    //Si on recoit un message d'erreur ou si une trame est perdue
                    if(tReponse.getType() == Type.R || tramePerdu){

                        //On réajuste les fenêtres
                        int diff = (tReponse.getNum() - pointeurTrame + 8) % 8;
                        pointeurFenetre += diff;
                        pointeurTrame = tReponse.getNum();
                        break;
                    }

                } catch (SocketTimeoutException e){

                    //Trame perdue
                    t = new Trame(Type.P, 0, "");
                    printToBuffer(t);
                    tramePerdu = true;

                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            //Si le dernier paquets a été envoyé et si ce dernier paquet a été reçu
            //On tente de mettre fin à la communication
            if(endSent && endConfirm){
                //Demande de fin de connexion
                t = new Trame(Type.F, 0, "");
                printToBuffer(t);

                //Attend la confirmation
                try {
                    t = new Trame(br.readLine());
                    System.out.println("IN :\t" + t.log());
                }catch (Exception e){
                    System.out.println(e);
                }

                if(t.getType() == Type.F){
                    connect = false;
                }
            }
        }
    }

    /**
     * Fonction qui print la trame dans le buffer et imprime le log OUT
     * @param t Trame à imprimer
     */
    private static void printToBuffer(Trame t){
        pw.println(t);
        pw.flush();
        System.out.println("OUT :\t" + t.log());
    }
}
