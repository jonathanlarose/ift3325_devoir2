import java.util.Random;

/**
 * IFT3325 - Téléinformatique
 * Devoir 2
 * Automne 2017
 *
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

/**
 * Classe utilisée pour le test du bit stuffing et de la validation du CRC des trames
 */
public class Tests {

    public static void main(String[] args) {

        for (int i=0; i<100; i++) {

            // Choix aléatoire du type de la trame
            Type type = null;
            int p1 = new Random().nextInt(6)+1;

            switch (p1) {
                case 1: type = Type.I;
                        break;
                case 2: type = Type.C;
                        break;
                case 3: type = Type.A;
                        break;
                case 4: type = Type.R;
                        break;
                case 5: type = Type.F;
                        break;
                case 6: type = Type.P;
                        break;
            }

            // Choix aléatoire du numéro de la trame
            int num = new Random().nextInt(8);

            // Creer des données (chaine binaire) pour le champs data
            String data = "";
            if (new Random().nextInt(100)+1 > 10) { // 10% de chance d'envoyer un message vide
                for (int j=0; j<50; j++) {
                    if (new Random().nextInt(100)+1 > 30) {
                        data += "1";
                    } else {
                        data += "0";
                    }
                }
            }


            // Trame originale (Envoie) sans erreurs
            Trame trameEnvoie = new Trame(type, num, data);
            System.out.println("Type+num+data+crc (sans bit stuffing) : " + trameEnvoie.getMessage()+trameEnvoie.getCrc());
            System.out.println("Bit stuffing                          : " + trameEnvoie.getBitStuffing());
            System.out.println("Trame complète (avec flags)           : " + trameEnvoie.getTrame());
            System.out.println("S(x)          : " + trameEnvoie.getCrc());
            System.out.println("Test M(x)S(x) : " + trameEnvoie.getTestCrc());

            // 50% de chance de corrompre la trame originale
            if (new Random().nextInt(100) + 1 > 50) {
                trameEnvoie.corruptTrame();
            }

            // Trame de reception
            Trame trameReception = new Trame(trameEnvoie.toString());

            // Comparaison des Test de M(x)S(x) avec le polynôme générateur
            if (trameReception.estValide()) {  // si trame intacte
                System.out.println("TRAME INTACTE !");
                System.out.println("\tTrame         : " + trameEnvoie.getTrame());
                System.out.println("\tS(x)          : " + trameReception.getCrc());
                System.out.println("\tTest M(x)S(x) : " + trameReception.getTestCrc()+"\n");

                // "Assert" - Les valeurs de S(x) doievent être égaux car message intacte
                if (!trameEnvoie.getTestCrc().equals(trameReception.getTestCrc())) {
                    System.out.println("ERREUR DANS LE CODE !");
                }
            } else {    // si trame corrompue
                System.out.println("TRAME CORROMPUE !");
                System.out.println("\tTrame         : " + trameEnvoie.getTrame());
                System.out.println("\tS(x)          : " + trameReception.getCrc());
                System.out.println("\tTest M(x)S(x) : " + trameReception.getTestCrc()+"\n");

                // "Assert" - Les valeurs de S(x) doievent différents égaux car message corrompue
                // pour trameReception S(x) != "0000000000000000"
                if (trameEnvoie.getTestCrc().equals(trameReception.getTestCrc())) {
                    System.out.println("ERREUR DANS LE CODE !");
                }
            }
        }
    }
}
