/**
 * Created by Jonathan on 2017-11-22.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;

public class Receiver {

    private static int numPort;
    private static  ArrayList<String> donnees;
    private static ServerSocket ss;
    private static Socket s;
    private static BufferedReader br;
    private static PrintWriter pw;
    private static boolean erreur = true;

    /**
     * Fonction executable du Receiver.
     * @param args Numero du port duquel on recoit le stream d'information.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {

        //Arguments Receiver
        donnees = new ArrayList<String>(0);

        try {
            numPort = Integer.parseInt(args[0]);

        } catch (Exception e) {
            System.out.println(e);
            return;
        }

        //Connection : si ca ne fonctionne pas, on quitte
        if(!connect())
            return;

        //Recoit les données puis déconnecte
        receive();
        deconnect();

        ParserTrames.assembleTrame(donnees, "new.txt");
    }

    /**
     * Méthode pour se connecter le socket au port duquel on veut recevoir les données
     * @return true si la connection est établie, false sinon
     */
    private static boolean connect(){

        //Connection au port
        try {
            ss = new ServerSocket(numPort);
            s = ss.accept();
            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            pw = new PrintWriter(s.getOutputStream());

            //Début de la communication
            Trame t = new Trame(br.readLine());
            System.out.println("IN :\t" + t.log());
            if(t.getType() == Type.C) {
                Trame response = new Trame(Type.A, 0, "");
                printToBuffer(response);
                System.out.print("\n");
            }
            else
                throw new TrameException("Échec de la connexion pour envoie");

        }catch (Exception e){
            System.out.println(e);
            return false;
        }

        return true;
    }

    /**
     * Méthode pour fermer la connection et les buffers
     */
    private static void deconnect(){

        //Fin de la communication
        try {
            br.close();
            pw.close();
            ss.close();
            s.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * Fonction qui s'occupe de la communication avec l'autre machine.
     * Elle s'occupe de recevoir les informations des trames transmises.
     * Le data valide est ajouté dans l'attribut statique "donnees" de la classe.
     */
    private static void receive()
    {
        //Reception des paquets
        int numTrame = 0;
        boolean connect = true;     //True tant qu'on a encore des données à recevoir
        boolean detruit = false;    //True :  on détruit tous les paquets reçus juqsqu'à ce qu'on reçoive le bon

        while(connect){

            try {
                String in = br.readLine();
                Trame t = new Trame(in);
                System.out.println("IN :\t" + t.log());

                //Si la trame est corrompue
                if(!t.estValide())
                    throw new TrameException("");

                //Si la trame recue est une trame d'information (I)
                if(t.getType() == Type.I){

                    //Si c'est la trame attendue
                    if(t.getNum() == numTrame){
                        donnees.add(t.getData());
                        numTrame = (numTrame + 1) %8;

                        Trame reponse = new Trame(Type.A, numTrame, "");


                        //Bug Corrupt
                        if(Math.floor(Math.random()*16) == 7 && erreur)
                            reponse.corruptTrame();

                        //Bug paquet perdu
                        if(Math.floor(Math.random()*16) == 11 && erreur) {
                            System.out.println("OUT :\t" + reponse.log());
                            continue;
                        }

                        printToBuffer(reponse);

                        //Au cas où on était en mode destruction de paquet, on peut arrêter de détruire les prochain paquets
                        detruit = false;
                    }
                    //Si ce n'est pas le numero de trame attendu
                    else{
                        throw new TrameException("");
                    }
                }
                //Si la trame recue est une trame P bit (P)
                else if(t.getType() == Type.P){
                    Trame reponse = new Trame(Type.A, numTrame, "");
                    printToBuffer(reponse);
                }
                //Si la trame recue est une trame de demande de fin de communication (F)
                else if(t.getType() == Type.F){
                    Trame reponse = new Trame(Type.F, numTrame, "");
                    printToBuffer(reponse);
                    connect = false;
                }


            }catch (TrameException e){

                if(!detruit){
                    Trame reponse = new Trame(Type.R, numTrame, "");
                    printToBuffer(reponse);

                    //On détruit les prochains paquets tant qu'on ne recoit pas la bonne trame
                    detruit = true;
                }

                System.out.println("\t\tPaquet détruit...");

            }catch (Exception e){
                System.out.println(e);
                return;
            }
        }
    }

    /**
     * Fonction qui print la trame dans le buffer et imprime le log OUT
     * @param t Trame à imprimer
     */
    private static void printToBuffer(Trame t){
        pw.println(t);
        pw.flush();
        System.out.println("OUT :\t" + t.log());
    }
}
