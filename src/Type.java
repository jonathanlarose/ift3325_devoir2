/**
 * IFT3325 - Téléinformatique
 * Devoir 2
 * Automne 2017
 *
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

/**
 * Classe enum qui englobe les types possibles de trame
 */
public enum Type {
    I, C, A, R, F, P
}
