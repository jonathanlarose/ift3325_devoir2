/**
 * IFT3325 - Téléinformatique
 * Devoir 2
 * Automne 2017
 *
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

import java.util.Random;

/**
 * Classe qui gère les composants d'une trame (flags, type, num, données, CRC)
 */
public class Trame {

    private String flag = "01111110";
    private Type type = null;
    private int num = 0;
    private String data = "";
    private String crc = "";
    private String message = "";
    private String bitStuffing = "";
    private String trame = "";
    private boolean trameValide;
    private String testCrc = "";

    //Getters
    public int getNum() {
        return num;
    }

    public String getData() {
        return data;
    }

    public Type getType() {
        return type;
    }

    // Getters pour la classe Tests
    public String getTrame() { return trame; }

    public String getBitStuffing() { return bitStuffing; }

    public String getCrc() { return crc; }

    public String getTestCrc() { return testCrc; }

    public String getMessage() { return message; }

    /**
     * Constructeur de trame utilisé par Sender
     * @param type : Enum de la trame (I, C, A, R, F, P)
     * @param num : numéro intier entre 0 et 7
     * @param data : string des données
     */
    public Trame(Type type, int num, String data) {

        message = createMessage(type,num,data);

        this.type = type;
        this.num = num;
        this.data = data;
        this.crc = computeCRC(message);
        this.testCrc = computeCRC(message+crc);
        this.bitStuffing = insertBitStuffing(message+crc);
        this.trame = flag+bitStuffing+flag;
        this.trameValide = true;
    }

    /**
     * Constructeur de trame utilisé par Receiver
     * @param trame : le string avec les bits de la trame reçue
     */
    public Trame(String trame) {
        boolean[] validationTrame = validerTrame(trame);
        this.trameValide = validationTrame[0];
        this.trame = trame;

        // Il n'y a pas un problème de flag ou un message trop court (<43 bits)
        if (!validationTrame[1]) {

            this.bitStuffing = trame.substring(8, trame.length()-8);
            String messagePlusCRC = removeBitStuffing(bitStuffing);
            this.testCrc = this.computeCRC(messagePlusCRC);

            // Valider intégrité du message avec CRC
            if(testCrc.equals("0000000000000000")) {
                String typeS = messagePlusCRC.substring(0,8);
                char typeChar = (char) Integer.parseInt(typeS,2);
                switch (typeChar) {
                    case 'I':   this.type = Type.I;
                        break;
                    case 'C':   this.type = Type.C;
                        break;
                    case 'A':   this.type = Type.A;
                        break;
                    case 'R':   this.type = Type.R;
                        break;
                    case 'F':   this.type = Type.F;
                        break;
                    case 'P':   this.type = Type.P;
                        break;
                }
                String numS = messagePlusCRC.substring(8,11);
                this.num = Integer.parseInt(numS,2);
                this.data = messagePlusCRC.substring(11,messagePlusCRC.length()-16);
                this.crc = messagePlusCRC.substring(messagePlusCRC.length()-16,messagePlusCRC.length());
            } else {
                crc = "corrupted message, S(x) is not representative!";
            }

        } else {
            crc = "corrupted message, S(x) is not representative!";
            testCrc =  "corrupted flag or message with less than 43 bits. Impossible to verify CRC!";
        }
    }

    /**
     * Méthode toString de trame
     * @return : string trame avec flags, crc et bit stuffing
     */
    public String toString() {
        return trame;
    }

    /**
     * Méthode pour corrompre une trame
     */
    public void corruptTrame() {

        int p1 = new Random().nextInt(100)+1;
        int p2 = new Random().nextInt(100)+1;
        int p3 = new Random().nextInt(trame.length()-16);

        // 50% de chance de corrompre le flag ou taille du message et 50% de change de corrompre le corps du message
        if(p1>50) {          // si oui, corrompre flag
            if (p2>33) {
                if (p2>67) {    // Corrompre flag début (suppression 1 bit)
                    trame = trame.substring(1, trame.length());
                }else {         // Corrompre flag fin (changement dernier bit)
                    trame = trame.substring(0,trame.length()-1)+"1";
                }
            } else {            // Taille du message <43 bits
                trame = trame.substring(0,10);
            }
        } else {            // sinon corrompre corps du message
            String changeBit = trame.substring(p3+8,p3+9);
            // Changer 0 pour 1 ou vice versa
            if (changeBit.equals("0")) {
                changeBit = "1";
                trame = trame.substring(0,p3+8)+changeBit+trame.substring(p3+9,trame.length());
            } else {
                changeBit = "0";
                trame = trame.substring(0,p3+8)+changeBit+trame.substring(p3+9,trame.length());
            }
        }
    }

    /**
     * Méthode pour informer si la trame est valide
     * @return : true si valide et false sinon
     */
    public boolean estValide() {
        return trameValide;
    }

    /**
     * Méthode pour le log des messages
     * @return string du log des messages
     */
    public String log() {

        if(!estValide())
            return "CORRUPTED";

        String log = "";
        switch (type) {
            case I: log += "I," + num + ",0";
                    break;
            case C: log += "C," + num;
                    break;
            case A: log += "RR," + num;
                    break;
            case R: log += "REJ," + num;
                    break;
            case F: log += "F," + num;
                    break;
            case P: log += "P";
                    break;
            default:
                break;
        }

        return log;
    }

    /**
     * Transforme les champs type, num et données d'une trame en une string binaire
     * @param type : Enum de la trame (I, C, A, R, F, P)
     * @param num : numéro intier de 0 à 7
     * @param data : string des données
     * @return : string avec type+num+data en binaire
     */
    private String createMessage(Type type, int num, String data) {

        String message = "";

        // convertion en binaire
        int typeCharCode = Character.codePointAt(type.toString(), 0);
        String typeBin = Integer.toBinaryString(typeCharCode);        // convertion en binaire

        // Ajout de zéros jusque à la taille de 8 bits
        int paddingHuit = 8 - typeBin.length();
        for (int i=0; i<paddingHuit; i++) {
            typeBin = "0"+typeBin;
        }

        String numBin = Integer.toBinaryString(num);       // convertion en binaire

        // Ajout de zéros jusque à la taille de 3 bits
        int paddingTrois = 3 - numBin.length();
        for (int i=0; i<paddingTrois; i++) {
            numBin = "0"+numBin;
        }

        message += typeBin + numBin + data;

        return message;
    }

    /**
     * Calculer le CRC de la trame
     * @param message : string en binaire de type+num+data
     * @return : valeur de S(x) à être ajoutée à la fin du message
     */
    private String computeCRC(String message) {
        int c0 = 0, c1 = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0, c7 = 0,
                c8 = 0, c9 = 0, c10 = 0, c11 = 0, c12 = 0, c13 = 0, c14 = 0, c15 = 0;
        int c0pred = 0, c1pred = 0, c2pred = 0, c3pred = 0, c4pred = 0, c5pred = 0, c6pred = 0, c7pred = 0,
                c8pred = 0, c9pred = 0, c10pred = 0, c11pred = 0, c12pred = 0, c13pred = 0, c14pred = 0, c15pred = 0;

        String crc = "";

        // Calcul du CRC avec le polynôme générateur CRC-CCITT (x16 + x12 + x5 + 1) avec la méthode Shift register
        int c15XORc11XORI = 0, c15XORc4XORI = 0,c15XORI = 0, i = 0;

        for (int j=0; j<=message.length(); j++) {

            if (j < message.length()) {     // pas exécuter la dernière itération
                i = Integer.parseInt(message.substring(j,j+1));
            }

            // On change les valeurs de c0 à c15 à partir de la deuxième itération
            if (j>0) {
                c0 = c15XORI;
                c1 = c0pred;
                c2 = c1pred;
                c3 = c2pred;
                c4 = c3pred;
                c5 = c15XORc4XORI;
                c6 = c5pred;
                c7 = c6pred;
                c8 = c7pred;
                c9 = c8pred;
                c10 = c9pred;
                c11 = c10pred;
                c12 = c15XORc11XORI;
                c13 = c12pred;
                c14 = c13pred;
                c15 = c14pred;
            }

            // Calcul des XOR
            c15XORc11XORI = c15 ^ c11 ^ i;
            c15XORc4XORI = c15 ^ c4 ^ i;
            c15XORI = c15 ^ i;

            // Garder les résultat pour la prochaine itération
            c0pred = c0;
            c1pred = c1;
            c2pred = c2;
            c3pred = c3;
            c5pred = c5;
            c6pred = c6;
            c7pred = c7;
            c8pred = c8;
            c9pred = c9;
            c10pred = c10;
            c12pred = c12;
            c13pred = c13;
            c14pred = c14;
        }

        return crc = ""+c15+c14+c13+c12+c11+c10+c9+c8+c7+c6+c5+c4+c3+c2+c1+c0;
    }

    /**
     * Valider si la trame n'est pas corrompue
     * @param trameS : trame en binaire
     * @return : array de booleans qu'indique si la trame est valide et le type d'erreur sinon
     */
    private boolean[] validerTrame(String trameS) {
        // validation[0] état de validité de la trame
        // validation[1] erreur de flag ou trame trop petite (sans flags et entête)
        boolean[] validation = new boolean[2];

        // Tester taille minimal trame et flags
        if(trameS.length() < 43 ||
                !trameS.substring(0,8).equals("01111110") ||
                !trameS.substring(trameS.length()-8,trameS.length()).equals("01111110")) {
            validation[0] = false;  //trame invalide
            validation[1] = true;   // erreur dans l'entête
            return validation;
        } else {
            trameS = trameS.substring(8,trameS.length()-8);
        }

        // Enlever bit stuffing
        trameS = removeBitStuffing(trameS);

        // Tester CRC
        String testCrcValidation = computeCRC(trameS);

        if (!testCrcValidation.equals("0000000000000000")) {
            validation[0] = false;      // trame invalide
            validation[1] = false;      // pas d'erreur dans l'entête
            return validation;
        } else {
            validation[0] = true;       // trame valide
            validation[1] = false;      // pas d'erreur dans l'entête
            return validation;
        }
    }

    /**
     * Méthode pour l'insertion du bit stuffing
     * @param messagePlusCrc : M(x) + S(x)
     * @return : string du message avec le bit stuffing
     */
    private String insertBitStuffing(String messagePlusCrc) {
        String bitStuffing = "";            // string du message avec bit stuffing à la fin des opérations
        int compteurUns = 0;                // compteur pour surveiller les suites de plus de cinq uns

        for (int i=0; i<messagePlusCrc.length(); i++) {
            String chiffreActuel = messagePlusCrc.substring(i,i+1);

            if(compteurUns == 5) {                  // si 5 zéros de suite
                if(chiffreActuel.equals("1")) {     // si encore un 1
                    bitStuffing += "01";            // on insère un 0 dans le string du message
                    compteurUns = 1;                // on met le compteur à 1
                } else {                            // sinon
                    bitStuffing += "00";             // on insère le 0 dans le string du message
                    compteurUns = 0;                // on met le compteur à 0
                }

            } else {                                // si moin de 5 zéros de suite
                if(chiffreActuel.equals("1")) {     // si bit actuel est un 1
                    bitStuffing += "1";             // on insère le 1 dans le string du message
                    compteurUns++;                  // on augment le compteur de 1
                } else {                            // sinon
                    bitStuffing += "0";             // on insère le 0 dans le string du message
                    compteurUns = 0;                // on met le compteur à 0
                }
            }
        }

        return bitStuffing;
    }

    /**
     * Méthode pour la rémotion du bit stuffing
     * @param bitStuffing : string du message (sans flags) avec le bit stuffing
     * @return : message sans le bit stuffing
     */
    private String removeBitStuffing(String bitStuffing) {
        String sansBitStuffing = "";            // string du message sans bit stuffing à la fin des opérations
        int compteurUns = 0;                    // compteur pour surveiller les suites de plus de cinq uns

        for (int i = 0; i < bitStuffing.length(); i++) {
            String chiffreActuel = bitStuffing.substring(i, i + 1);

            if (compteurUns == 5) {                  // si 5 zéros de suite
                compteurUns = 0;                    // on met le compteur à 0 et on n'insère rien dans le string du message

            } else {                                // si moin de 5 zéros de suite
                if (chiffreActuel.equals("1")) {     // si bit actuel est un 1
                    sansBitStuffing += "1";             // on insère le 1 dans le string du message
                    compteurUns++;                  // on augment le compteur de 1
                } else {                            // sinon
                    sansBitStuffing += "0";         // on insère le 0 dans le string du message
                    compteurUns = 0;                // on met le compteur à 0
                }
            }
        }

        return sansBitStuffing;
    }
}
