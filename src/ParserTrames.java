import java.io.*;
import java.util.ArrayList;

/**
 * IFT3325 - Téléinformatique
 * Devoir 2
 * Automne 2017
 *
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */


/**
 * Classe qui gère la lecture et écriture des fichier pour l'extraction du champ données des trames
 */
public class ParserTrames {

    /**
     * Méthode pour parser un fichier .txt et extraire des données binaires pour les trames
     * @param fileName : nom du fichier à parser
     * @return un ArrayList<String> contenant les données à utiliser pour chaque trame
     * @throws IOException
     */
    public static ArrayList<String> parseTrames(String fileName) throws IOException {
        ArrayList<String> listeTrames = new ArrayList<String>(0);
        BufferedReader br = new BufferedReader(new FileReader(fileName));

        try {
            int nextCharFile;
            String dataTrame = "";
            int longueurTrame = 0;
            String charBin = "";

            while((nextCharFile = br.read()) != -1) {

                // Conversion en string binaire
                charBin = Integer.toBinaryString(nextCharFile);

                // Ajout de zéros jusque à la taille de 16 bits
                int padding = 16 - charBin.length();
                for (int i=0; i<padding; i++) {
                    charBin = "0"+charBin;
                }


                // Division en trames de 800 bits
                if (longueurTrame%800 < 799) {
                    dataTrame += charBin;
                    longueurTrame++;
                } else {
                    dataTrame += charBin;
                    longueurTrame++;
                    listeTrames.add(dataTrame);
                    dataTrame = "";
                }
            }

            listeTrames.add(dataTrame);

        } finally {
            br.close();
        }
        return listeTrames;
    }

    /**
     * Assembler les champs données des trames et créer le fichier de réception
     * @param tabTrame : ArrayList avec les champs données des trames
     * @param fileName : nom du fichier à être écrit
     */
    public static void assembleTrame(ArrayList<String> tabTrame, String fileName) {
        String sortieTexte =  "";

        if (!tabTrame.isEmpty()) {      // si le fichier n'était pas vide au départ


            int j = 0;

            for (String s : tabTrame) {

                // parser les string du ArrayList de 16 en 16 bits (1 caractère)
                for (int i = 0; i < s.length(); i += 16) {

                    // créer le string à être écrit dans le fichier de réception
                    sortieTexte += (char) Integer.parseInt(s.substring(i, i + 16), 2);
                }
            }
        }

        Writer writer = null;

        // gérer l'écriture du fichier
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("reception_"+fileName)));
            writer.write(sortieTexte);
            writer.close();

        } catch (IOException e) {
            System.out.println("Erreur dans la création du fichier !");
        }
    }
}
